#import ParaMapFunctions as pm
import ParaMapFunctionsParallel as pm
import sys, os, glob
import nibabel as nib
from datetime import datetime
import numpy as np
#from matplotlib import pyplot as plt
#import xlrd
#import sys, os
#import SimpleITK as sitk

#######Running this code ###########
# 1. remove all the *.mevis.* data in folder
# 2. run from command example: #python RunPreMask.py EPIC7 3DXMLs /scratch/users/elkaffas/ParaMap/LSBolusAV/m267/20150319104853.535_boulous

# Can also be run in batches in sherlock using: sbatch PreMask.batch 411 (needs PreMask.batch to be uploaded in same folder)
####################################

if __name__ == "__main__":
	# CODE ACTIVATOR
	print('Started:');print(sys.argv[3]);print(str(datetime.now()));
	print('Note that a -Masks- directory must exist as a subdirectory containing masks.')
	print('This version of the code has only been tested with 4DNIFTI')
	path = os.path.normpath(sys.argv[3]);
	splitpath = path.split(os.sep);
	pathonly = os.path.dirname(path)

	# Check which system we're dealing with
	if sys.argv[1] == 'EPIC7':
		compressfactor = 24.09; #24.09; #42.98
	elif sys.argv[1] == 'iU22':
		compressfactor = 42.98;
	else:
		print('TERMINATED: First argument can only be EPIC7 or iU22');
		sys.exit(0);

	# Check which data format we are dealing with
	if sys.argv[2] == '4DNIFTI':
		fullname = splitpath[-1];
		name = fullname[0:6];print(name);
		day = fullname[7:13];print(day);
		format = '4DNIFTI'
	elif sys.argv[2] == '3DXMLs':
		name = splitpath[-2];
		day = splitpath[-1];day = day[0:8];
		format = '3DXMLs'
	else:
		print('TERMINATED: Second argument can only be 4DNIFTI or 3DXMLs');
		sys.exit(0);

	# These will be future options in code input
	testflag = 'no'; # Activate test mode
	automaskflag = 'no'; # Flag to indicate auto-masking yes/no >> barely used here
	manualmaskflag = 'no';
	autoresflag = 'yes';
	type = 'Bolus'
	fit = 'Lognormal'
	if type == 'Bolus':
		parameters = ['PE','AUC','TP','MTT','T0']

	# Read data	
	imarray, orgres, newres, time, imarray_org, mask = pm.prep_img(sys.argv[3],type,format, automaskflag, manualmaskflag, autoresflag, name, day);
	print('Done 3D to 4D:');print(str(datetime.now()));

	del imarray_org, orgres, mask;

	# # Create linearized 4D from original
	# print('Linearizing Signal Intenities:');print(str(datetime.now()));
	# imarray_lin=np.array(imarray_org);
	# imarray_lin[imarray_lin==0]= -100*compressfactor;
	# imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	# imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.
	
	# # Save the 4D lin image without masking
	# print('Saving 4D lin w/o mask:');print(str(datetime.now()));
	# #imarray_swap = np.reshape(imarray_org[0,:,0,:,:,:],(imarray_org.shape[1], imarray_org.shape[3], imarray_org.shape[4],imarray_org.shape[5]));
	# imarray_org2 = np.squeeze(imarray_lin);
	# imarray_org2 = imarray_org2.swapaxes(0,3);imarray_org2 = imarray_org2.swapaxes(1,2);
	# affine = np.eye(4)
	# niiarray = nib.Nifti1Image(imarray_org2.astype('uint8'),affine);
	# niiarray.header['pixdim'] = [4.,orgres[0], orgres[1], orgres[2], time, 0., 0., 0.];
	# #niiarray.header['slice_duration'] = time;
	# nib.save(niiarray, (name + '-' + day + type + 'LinearizedFull4D.nii.gz'));
	# del imarray_org2;

	# Save the 4D image without masking
	print('Saving processed 4D w/o mask:');print(str(datetime.now()));
	#imarray_swap = np.reshape(imarray_org[0,:,0,:,:,:],(imarray_org.shape[1], imarray_org.shape[3], imarray_org.shape[4],imarray_org.shape[5]));
	imarray2 = np.squeeze(imarray);
	imarray2 = imarray2.swapaxes(0,3);imarray2 = imarray2.swapaxes(1,2);
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray2.astype('uint8'),affine);
	niiarray.header['pixdim'] = [4.,newres[0], newres[1], newres[2], time, 0., 0., 0.];
	#niiarray.header['slice_duration'] = time;
	nib.save(niiarray, (name + '-' + day + type + 'Full4D.nii.gz'));
	del imarray2;

	del imarray;
	print('Done All');print(str(datetime.now()));